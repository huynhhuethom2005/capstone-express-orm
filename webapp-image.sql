-- Adminer 4.8.1 MySQL 8.0.33 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `_prisma_migrations`;
CREATE TABLE `_prisma_migrations` (
  `id` varchar(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `checksum` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `finished_at` datetime(3) DEFAULT NULL,
  `migration_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logs` text COLLATE utf8mb4_unicode_ci,
  `rolled_back_at` datetime(3) DEFAULT NULL,
  `started_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `applied_steps_count` int unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `_prisma_migrations` (`id`, `checksum`, `finished_at`, `migration_name`, `logs`, `rolled_back_at`, `started_at`, `applied_steps_count`) VALUES
('dfa215ea-7e88-45c9-8d22-2d3127e6d37a',	'ffbdf314ac183563f48b5dada235894a0f7a1711287380c76d73449c26ae7ccc',	'2023-06-28 02:34:44.673',	'20230628023444_',	NULL,	NULL,	'2023-06-28 02:34:44.500',	1);

DROP TABLE IF EXISTS `binh_luan`;
CREATE TABLE `binh_luan` (
  `binh_luan_id` int NOT NULL AUTO_INCREMENT,
  `nguoi_dung_id` int DEFAULT NULL,
  `hinh_id` int DEFAULT NULL,
  `ngay_binh_luan` date DEFAULT NULL,
  `noi_dung` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`binh_luan_id`),
  KEY `hinh_id` (`hinh_id`),
  KEY `nguoi_dung_id` (`nguoi_dung_id`),
  CONSTRAINT `binh_luan_ibfk_1` FOREIGN KEY (`nguoi_dung_id`) REFERENCES `nguoi_dung` (`nguoi_dung_id`),
  CONSTRAINT `binh_luan_ibfk_2` FOREIGN KEY (`hinh_id`) REFERENCES `hinh_anh` (`hinh_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `binh_luan` (`binh_luan_id`, `nguoi_dung_id`, `hinh_id`, `ngay_binh_luan`, `noi_dung`) VALUES
(1,	6,	1,	NULL,	'Hình này xấu quá'),
(2,	3,	1,	NULL,	'Dep'),
(3,	3,	2,	NULL,	'Xin anh'),
(4,	3,	2,	'2023-06-28',	'abc ysn'),
(5,	3,	2,	'2023-06-28',	'abc ysn');

DROP TABLE IF EXISTS `hinh_anh`;
CREATE TABLE `hinh_anh` (
  `hinh_id` int NOT NULL AUTO_INCREMENT,
  `ten_hinh` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `duong_dan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mo_ta` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nguoi_dung_id` int DEFAULT NULL,
  PRIMARY KEY (`hinh_id`),
  KEY `nguoi_dung_id` (`nguoi_dung_id`),
  CONSTRAINT `hinh_anh_ibfk_1` FOREIGN KEY (`nguoi_dung_id`) REFERENCES `nguoi_dung` (`nguoi_dung_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `hinh_anh` (`hinh_id`, `ten_hinh`, `duong_dan`, `mo_ta`, `nguoi_dung_id`) VALUES
(1,	'Hình ảnh 1',	NULL,	NULL,	3),
(2,	'Hinh anh 2',	NULL,	NULL,	4),
(4,	'1688002298210_1220491.jpg',	'/public/images',	NULL,	3),
(5,	'1688002345981_1220491.jpg',	'/public/images',	NULL,	3),
(6,	'1688002371793_1220491.jpg',	'/public/images',	NULL,	NULL),
(7,	'1688002469929_1220491.jpg',	'/public/images',	NULL,	3),
(8,	'1688002511995_beautiful-nature-2739115.jpg',	'/public/images',	NULL,	3),
(9,	'1688002532548_beautiful-nature-2739115.jpg',	'/public/images',	NULL,	3),
(10,	'1688002613779_1220491.jpg',	'/public/images',	NULL,	3);

DROP TABLE IF EXISTS `luu_anh`;
CREATE TABLE `luu_anh` (
  `nguoi_dung_id` int NOT NULL,
  `hinh_id` int NOT NULL AUTO_INCREMENT,
  `ngay_luu` date DEFAULT NULL,
  PRIMARY KEY (`hinh_id`),
  KEY `nguoi_dung_id` (`nguoi_dung_id`),
  CONSTRAINT `luu_anh_ibfk_1` FOREIGN KEY (`nguoi_dung_id`) REFERENCES `nguoi_dung` (`nguoi_dung_id`),
  CONSTRAINT `luu_anh_ibfk_2` FOREIGN KEY (`hinh_id`) REFERENCES `hinh_anh` (`hinh_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `luu_anh` (`nguoi_dung_id`, `hinh_id`, `ngay_luu`) VALUES
(3,	2,	NULL);

DROP TABLE IF EXISTS `nguoi_dung`;
CREATE TABLE `nguoi_dung` (
  `nguoi_dung_id` int NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mat_khau` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ho_ten` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tuoi` int DEFAULT NULL,
  `anh_dai_dien` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`nguoi_dung_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `nguoi_dung` (`nguoi_dung_id`, `email`, `mat_khau`, `ho_ten`, `tuoi`, `anh_dai_dien`) VALUES
(3,	'vvvv@gmail.com',	'$2b$10$7r7JIAVLEhGEwzL1WjKjBeOPVXW7yeo0WXtiAjSewOLbAbKs1OME.',	'VVVVV',	24,	''),
(4,	'1234564@gmail.com',	'$2b$10$bzzk9tQ0YLH8KmwhxwKbiurT3hv.4LrgdHLnp8SWtR2Ni6GbGMAqq',	'abc',	23,	NULL),
(5,	'abc@gmail.com',	'$2b$10$Cv.mJkKnc.Ll3NuJL/hAzeHpVcyWQNZ9nSWi13AJRZn0I6L8Zj3sa',	'abc',	23,	NULL),
(6,	'nguyenvana@gmail.com',	'$2b$10$O84xhV/gMQNGehlTpCAJm.jumIhjVVOp2yLsp9ZiZl9E7NA.BpCrC',	'Nguyen Van A',	23,	NULL),
(7,	'nguyenvanb@gmail.com',	'$2b$10$wIFz7.PEoidQuDnTDKDRhu3xv75DbNJDXIlUoUDWg1UMu2NTkHpHK',	'Nguyen Van B',	23,	NULL),
(8,	'nguyenvanc@gmail.com',	'$2b$10$YVd.nFz0PJ8qyyKXdX/LyeJBzEjBWlJWrpDJ4A7XzPUHfFmiMG/0.',	'Nguyen Van C',	23,	NULL),
(9,	'nguyenvand@gmail.com',	'$2b$10$M4PUPxSXwTuuCKOmJeDL4ubxPbyiIjugyvVWawyu2oyJEUZSlxQnq',	'Nguyen Van D',	23,	NULL),
(10,	'nguyenvane@gmail.com',	'$2b$10$FYHTL0aJD3QbAaRPqpdHwe47mH3atTNKJdgFOyIdtaz9nMq9WB2aC',	'Nguyen Van E',	23,	NULL);

-- 2023-06-29 02:04:21
