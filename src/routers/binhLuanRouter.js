import express from "express";
import { postBinhLuan } from "../controllers/binhLuanController.js";

const binhLuanRouter = express.Router()

binhLuanRouter.post('/', postBinhLuan)

export { binhLuanRouter }
