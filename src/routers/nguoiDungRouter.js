import express from "express"
import { getAll, getInforUser, updateUserInfor } from "../controllers/nguoiDungController.js"

const nguoiDungRouter = express.Router()

nguoiDungRouter.get('/', getAll)
nguoiDungRouter.get('/get-user-infor', getInforUser)
nguoiDungRouter.put('/update', updateUserInfor)

export { nguoiDungRouter }
