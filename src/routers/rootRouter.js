import express from "express";
import { nguoiDungRouter } from "./nguoiDungRouter.js";
import { authRouter } from "./authRouter.js";
import { hinhAnhRouter } from "./hinhAnhRouter.js";
import { luuAnhRouter } from "./luuAnhRouter.js";
import { binhLuanRouter } from "./binhLuanRouter.js";
import { verifyJwt } from "../config/jwt.js";
import { uploadRouter } from "./uploadRouter.js";

const rootRouter = express.Router()

rootRouter.use("/nguoi-dung", verifyJwt, nguoiDungRouter)
rootRouter.use("/auth", authRouter)
rootRouter.use("/hinh-anh", verifyJwt, hinhAnhRouter)
rootRouter.use("/luu-anh", verifyJwt, luuAnhRouter)
rootRouter.use("/binh-luan", verifyJwt, binhLuanRouter)
rootRouter.use("/upload", verifyJwt, uploadRouter)

export { rootRouter }
