import express from "express";
import { deleteHinhAnh, getAllHinhAnh, getBinhLuanIdAnh, getHinhAnhId, getHinhAnhUserId } from "../controllers/hinhAnhController.js";

const hinhAnhRouter = express.Router()

hinhAnhRouter.get('/get-all/', getAllHinhAnh)
hinhAnhRouter.get('/get-anh/:hinh_id', getHinhAnhId)
hinhAnhRouter.get('/binh-luan/:hinh_id', getBinhLuanIdAnh)
hinhAnhRouter.get('/get-user', getHinhAnhUserId)
hinhAnhRouter.delete('/delete/:hinh_id', deleteHinhAnh)

export { hinhAnhRouter }
