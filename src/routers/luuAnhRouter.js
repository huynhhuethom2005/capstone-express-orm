import express from "express";
import { checkLuuAnh, getLuuAnhUser } from "../controllers/luuAnhController.js";

const luuAnhRouter = express.Router()

luuAnhRouter.get('/check/:hinh_id', checkLuuAnh)
luuAnhRouter.get('/get-user', getLuuAnhUser)

export { luuAnhRouter }
