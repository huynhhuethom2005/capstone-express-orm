import express from "express";
import multer from "multer";
import { decodeToken } from "../config/jwt.js";
import { errorCode, successCode } from "../config/response.js";
import { PrismaClient } from '@prisma/client'

const prisma = new PrismaClient()

const uploadRouter = express.Router()

const storage = multer.diskStorage({
    destination: process.cwd() + "/public/images",
    filename: (req, file, callback) => {
        let newName = new Date().getTime() + "_" + file.originalname
        callback(null, newName)
    }
})

const upload = multer({ storage })

uploadRouter.post('/', upload.single("file"), async (req, res) => {
    try {
        let file = req.file
        let { token } = req.headers

        let { nguoi_dung_id } = decodeToken(token).data
        let dataCreate = {
            ten_hinh: file.filename,
            duong_dan: "/public/images",
            nguoi_dung_id
        }

        await prisma.hinh_anh.create({ data: dataCreate })

        successCode(res, dataCreate, "file successfully uploaded")
    } catch (e) {
        errorCode(res, "", "Server Error!!!")
    }
})

export { uploadRouter }
