import express from 'express';
import { rootRouter } from './routers/rootRouter.js';

const app = express();
app.use(express.json())
app.use(express.static("."))

app.listen(8081)

app.use('/api', rootRouter)
