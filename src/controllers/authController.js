import { PrismaClient } from '@prisma/client'
import { failCode, successCode } from '../config/response.js'
import bcrypt from 'bcrypt'
import { generateToken } from '../config/jwt.js'

const prisma = new PrismaClient()

const userSignup = async (req, res) => {
    try {
        let userRes = req.body

        let checkEmail = await prisma.nguoi_dung.findFirst({
            where: { email: userRes.email }
        })

        if (checkEmail) {
            failCode(res, userRes.email, "Email already exists")
        } else {
            let newUser = { ...userRes, tuoi: userRes.tuoi * 1, mat_khau: bcrypt.hashSync(userRes.mat_khau, 10) }
            await prisma.nguoi_dung.create({ data: newUser })
            successCode(res, "", "Signed in successfully")
        }
    } catch (e) {
        failCode(res, e.name, "Signed in failed")
    }
}

const userLogin = async (req, res) => {
    let { email, mat_khau } = req.body

    let checkUser = await prisma.nguoi_dung.findFirst({
        where: { email }
    })

    if (checkUser) {
        if (bcrypt.compareSync(mat_khau, checkUser.mat_khau)) {
            let token = generateToken(checkUser)
            let { mat_khau, ...userRes } = { ...checkUser, token }
            successCode(res, userRes, "Login successful!")
        } else {
            failCode(res, email, "Password mismatch!")
        }
    } else {
        failCode(res, email, "User not found!")
    }
}

export { userSignup, userLogin }
