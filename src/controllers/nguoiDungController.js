import { PrismaClient } from '@prisma/client'
import { successCode } from '../config/response.js'
import { decodeToken } from '../config/jwt.js'
import bcrypt from 'bcrypt'

const prisma = new PrismaClient()

const getAll = async (req, res) => {
    let data = await prisma.nguoi_dung.findMany()
    successCode(res, data, "Get nguoi_dung successfully!")
}

const getInforUser = async (req, res) => {
    try {
        let { token } = req.headers
        let userInfor = decodeToken(token)
        let { mat_khau, ...dataRes } = { ...userInfor.data }
        successCode(res, dataRes, "Get infor_user successfully!")
    } catch (e) {
        errorCode(res, "", "Server Error!!!")
    }
}

const updateUserInfor = async (req, res) => {
    try {
        let { token } = req.headers
        let { nguoi_dung_id } = decodeToken(token).data
        let dataRes = req.body

        let dataUpdate = { ...dataRes, mat_khau: bcrypt.hashSync(dataRes.mat_khau, 10) }

        await prisma.nguoi_dung.update({
            where: { nguoi_dung_id },
            data: dataUpdate
        })

        successCode(res, dataUpdate, "Update infor_user successfully!")
    } catch (e) {
        errorCode(res, e.message, "Server Error!!!")
    }
}

export { getAll, getInforUser, updateUserInfor }
