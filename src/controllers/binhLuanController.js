import { PrismaClient } from '@prisma/client'
import { errorCode, successCode } from '../config/response.js'
import { decodeToken } from '../config/jwt.js'

const prisma = new PrismaClient()

const postBinhLuan = async (req, res) => {
    try {
        let { token } = req.headers
        let { hinh_id, noi_dung } = req.body
        let userInfor = decodeToken(token)

        let dataCreate = {
            nguoi_dung_id: userInfor.data.nguoi_dung_id,
            hinh_id: hinh_id * 1,
            ngay_binh_luan: new Date(),
            noi_dung
        }

        await prisma.binh_luan.create({ data: dataCreate })

        successCode(res, dataCreate, "Created successfully!!!")
    } catch (e) {
        errorCode(res, "", "Server Error!!!")
    }
}

export { postBinhLuan }
