import { PrismaClient } from '@prisma/client'
import { errorCode, successCode } from '../config/response.js'
import { decodeToken } from '../config/jwt.js'

const prisma = new PrismaClient()

const getAllHinhAnh = async (req, res) => {
    try {
        let { searchKey, page, pageSize } = req.query

        let data = await prisma.hinh_anh.findMany({
            where: { ten_hinh: { contains: searchKey } },
            skip: (page - 1) * pageSize,
            take: pageSize * 1,
        })

        let totalData = await prisma.hinh_anh.count({
            where: { ten_hinh: { contains: searchKey } },
        })
        let totalPage = Math.ceil(totalData / pageSize * 1)
        let dataRes = { data, totalData, totalPage }

        successCode(res, dataRes, "Get All Results Success")
    } catch (e) {
        errorCode(res, "", "Server Error!!!")
    }
}

const getHinhAnhId = async (req, res) => {
    try {
        let { hinh_id } = req.params
        let data = await prisma.hinh_anh.findFirst({
            where: { hinh_id: hinh_id * 1 },
            include: {
                nguoi_dung: true
            }
        })
        successCode(res, data, "Get data successfully")
    } catch (e) {
        errorCode(res, "hekllo", "Server Error!!!")
    }
}

const getBinhLuanIdAnh = async (req, res) => {
    try {
        let { hinh_id } = req.params
        let data = await prisma.hinh_anh.findFirst({
            where: { hinh_id: hinh_id * 1 },
            include: {
                binh_luan:
                {
                    include: {
                        nguoi_dung: true
                    }
                }
            }
        })
        successCode(res, data, "Get data successfully")
    } catch (e) {
        errorCode(res, "", "Server Error!!!")
    }
}

const getHinhAnhUserId = async (req, res) => {
    try {
        let { token } = req.headers
        let userInfor = decodeToken(token)
        let data = await prisma.hinh_anh.findMany({
            where: {
                nguoi_dung_id: userInfor.data.nguoi_dung_id
            }
        })

        successCode(res, data, "Get data successfully")
    } catch (e) {
        errorCode(res, e.message, "Server Error!!!")
    }
}

const deleteHinhAnh = async (req, res) => {
    try {
        let { hinh_id } = req.params
        await prisma.hinh_anh.delete({ where: { hinh_id: hinh_id * 1 } })
        successCode(res, hinh_id, "Delete data successfully!!!")
    } catch (e) {
        errorCode(res, e.message, "Server Error!!!")
    }
}

export { getAllHinhAnh, getHinhAnhId, getBinhLuanIdAnh, getHinhAnhUserId, deleteHinhAnh }
