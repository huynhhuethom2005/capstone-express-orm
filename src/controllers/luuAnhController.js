import { PrismaClient } from '@prisma/client'
import { errorCode, successCode } from '../config/response.js'
import { decodeToken } from '../config/jwt.js'

const prisma = new PrismaClient()

const checkLuuAnh = async (req, res) => {
    try {
        let { token } = req.headers
        let { hinh_id } = req.params
        let userInfor = decodeToken(token)

        let checked = await prisma.luu_anh.findFirst({
            where: {
                nguoi_dung_id: userInfor.data.nguoi_dung_id,
                hinh_id: hinh_id * 1
            }
        })

        if (checked) {
            successCode(res, checked, "Have saved!")
        } else {
            successCode(res, "", "Unsaved!")
        }
    } catch (e) {
        errorCode(res, "", "Server Error!!!")
    }
}

const getLuuAnhUser = async (req, res) => {
    try {
        let { token } = req.headers
        let userInfor = decodeToken(token)
        let data = await prisma.luu_anh.findMany({
            where: {
                nguoi_dung_id: userInfor.data.nguoi_dung_id
            },
            include: {
                hinh_anh: true
            }
        })
        successCode(res, data, "Get data successfully")
    } catch (e) {
        errorCode(res, "", "Server Error!!!")
    }
}

export { checkLuuAnh, getLuuAnhUser }
