import jwt from "jsonwebtoken"

// Generate token
const generateToken = (data) => {
    let token = jwt.sign({ data }, "huynhhuethom", { algorithm: "HS256", expiresIn: "1h" })
    return token
}

// Check token
const checkToken = (token) => {
    return jwt.verify(token, "huynhhuethom")
}

//  decode token
const decodeToken = (token) => {
    return jwt.decode(token)
}

// Khoá API bằng token
const verifyJwt = (req, res, next) => {
    try {
        let { token } = req.headers
        let verifyToken = checkToken(token)
        if (verifyToken) {
            next()
        }
    }
    catch (err) {
        res.send(err.message)
    }
}

export { generateToken, checkToken, decodeToken, verifyJwt }
